﻿## C++ Genetic Algorithm for Solving TSP
This is a genetic algorithm for solving the [Traveling Salesman Problem](https://en.wikipedia.org/wiki/Travelling_salesman_problem) that I wrote in C++ using SFML.

## Notes on running the program:

 - The program uses SFML version 2.5.1 available [here](https://www.sfml-dev.org/download/sfml/2.5.1/).
 - I found this [SFML Tutorial](https://www.youtube.com/watch?v=axIgxBQVBg0&list=PL21OsoBLPpMOO6zyVlxZ4S4hwkY_SLRW9&ab_channel=HilzeVonck) very helpful for getting started.
 - You may need to move the .dll files located in SFML-2.5.1 > bin into your output folder in-order to get the program to run.
 - The algorithm has a 15 second delay before it starts this was to make recording easier.

## [Demo Video](https://www.youtube.com/watch?v=0joAOuyrY-s&ab_channel=JohnDodd)

[![See the algorithm in action.](http://img.youtube.com/vi/0joAOuyrY-s/0.jpg)](http://www.youtube.com/watch?v=0joAOuyrY-s "See the algorithm in action.")

## Attribution
Please provide proper attribution when reproducing or distributing this program. Please do not attempt to pass this program off as your own for a school assignment. 

**Created by John Dodd 10-08-2020**

