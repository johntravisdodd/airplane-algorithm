#include "Globals.h"

// Amount to reserve for Vectors
// This should generally be tied to or exceed
// The amount of nodes the program is dealing with
const int VECTOR_RESERVATION = 10000;