#pragma once

#include "DisplayObject.h"
#include "Node.h"
#include <gsl.h>

/// <summary>
/// Manages the drawing of a line between two nodes.
/// Tracks changes to node positions even if the nodes move.
/// </summary>
class Connection : public Displayobject
{
public:
	Connection(std::shared_ptr <Node> a, std::shared_ptr <Node> b);

	void
	update() override;

	void
	draw(sf::RenderWindow& window) override;

	sf::Vector2f
	get_center() override;

	void
	set_position(sf::Vector2f position) override;

	// Variables
	std::shared_ptr <Node> A;

	std::shared_ptr <Node> B;

private:

	sf::VertexArray Vertex_Array;

	sf::Color Color_a;

	sf::Color Color_b;

};

