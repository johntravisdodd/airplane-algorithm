#pragma once

#include <SFML\Graphics\RenderWindow.hpp>
#include <gsl.h>

/// <summary>
/// Abstract class for displaying objects.
/// </summary>
class Displayobject
{
public:
	/// <summary>
	/// Updates the state of this object. Should handle all processor-intensive operations.
	/// </summary>
	virtual void
	update() = 0;

	/// <summary>
	/// Called to draw the object in a window.
	/// </summary>
	/// <param name="window">A render window where the object will be drawn.</param>
	virtual void
	draw(sf::RenderWindow& window) = 0;

	/// <summary>
	/// Gets the center of this object.
	/// </summary>
	/// <returns>A Vector2 coordinate representing the object's center position.</returns>
	virtual sf::Vector2f
	get_center() = 0;

	/// <summary>
	/// Sets the position of this display object.
	/// </summary>
	/// <param name="position">The position to set this display object at (using upper left coordinate as a guide).</param>
	virtual void
	set_position(sf::Vector2f position) = 0;
};

