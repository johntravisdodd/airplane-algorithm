#pragma once

#include <vector>
#include "Node.h"
#include "Connection.h"
#include "DisplayObject.h"
#include "Level.h"
#include "Connection.h"
#include <gsl.h>

/// <summary>
/// A path stores a list of pointers which
/// point to nodes. A path can return a list of
/// connections which allows it to be represented
/// and drawn on screen. 
/// </summary>
class Path
{
public:

	// Functions
	Path(std::vector <std::shared_ptr<Node>> nodes, bool nearest_neighbor);

	Path(std::vector <std::shared_ptr<Node>> nodes, std::shared_ptr <Path> pathA, std::shared_ptr <Path> pathB);

	float
	get_length();

	std::vector <std::shared_ptr<Connection>>
	get_connectors();

	std::shared_ptr <Path>
	get_copy();

	std::shared_ptr <Node>
	longest_node();

	void
	remove_node(std::shared_ptr <Node> node);

	float
	longest_distance();

	void
	nearest_neighbor_pass(int amount);

	int
	get_position(std::shared_ptr <Node> node);

	std::shared_ptr <Node>
	get_closest_node(std::shared_ptr <Node> node);

	std::shared_ptr <Node>
	get_greatest_mismatch();

	void
	swap(int a, int b);

	// Variables
	std::vector <std::shared_ptr<Node>> path_nodes;

private:

	// Functions
	bool
	contains_pair(std::vector <std::shared_ptr<Node>> path, std::shared_ptr <Node> a, std::shared_ptr <Node> b);

	// Variables
	std::vector <std::shared_ptr<Connection>> connectors;

	std::shared_ptr <Level> main_level;
};

