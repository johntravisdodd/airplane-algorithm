#include <stdlib.h>
#include <SFML/Graphics.hpp>
#include <cmath>
#include <gsl.h>

namespace
{
	/// <summary>
	/// Returns a random SFML color.
	/// </summary>
	/// <returns>A random SFML color</returns>
	sf::Color
	get_random_color()
	{
		const int r = rand() % 255;
		const int g = rand() % 255;
		const int b = rand() % 255;
		return sf::Color(r, g, b);
	}

	/// <summary>
	/// Returns the distance between two Vector2f
	/// </summary>
	/// <param name="a">The first vector</param>
	/// <param name="b">The second vector</param>
	/// <returns>The distance between two points.</returns>
	float
	get_distance(sf::Vector2f a, sf::Vector2f b) noexcept
	{
		const float distance = sqrt(powf(b.x - a.x, 2) + powf(b.y - a.y, 2));
		return distance;
	}
}
