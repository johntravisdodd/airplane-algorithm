// Airplane-Algorithm.cpp : This file contains the 'main' function. Program execution begins and ends there.
// Created by John Dodd - 2020

#include <iostream>
#include <list>
#include <SFML/Graphics.hpp>
#include <stdlib.h>
#include <future>
#include <chrono>
#include "Level.h"
#include "Node.h"
#include "Connection.h"
#include "Utility.h"
#include "Path.h"
#include "GenePool.h"
#include <gsl.h>

/// <summary>
/// Holds the main level.
/// </summary>
static std::shared_ptr <Level> mainLevel;

/// <summary>
/// Holds the gene pool object for running
/// the GA algorithm.
/// </summary>
static std::shared_ptr <Genepool> gene_pool;

/// <summary>
/// The main entry point for our application.
/// </summary>
/// <returns>An integer.</returns>
int
main()
{
	// Determines if we place nodes in a circle or at random locations
	constexpr bool use_circle = false;
	constexpr double window_scale = 1.0; // Window scaling
	constexpr int startup_wait_time_in_seconds = 15; // 15 second wait time for screen capture
	constexpr int node_count = 35;

	// Setup start timer (useful for recording my screen)
	const sf::Clock clock;

	// Seed random number generation
	const unsigned seed = time(nullptr);

	// Seed the random number generator.
	srand(seed);

	// Get screen resolution
	const int screen_width = sf::VideoMode::getDesktopMode().width;
	const int screen_height = sf::VideoMode::getDesktopMode().height;

	// Create a render window
	sf::RenderWindow window(sf::VideoMode(screen_width * window_scale, screen_height * window_scale), "TSP-Algorithm");

	// Create a level
	mainLevel = std::make_shared<Level>();

	const double pi = (atan(1) * 4.0);
	const double degree = (pi * 2.0) / node_count;
	const double radius = screen_height / 2;
	// Generate random nodes
	for (int i = 0; i < node_count; i++)
	{
		std::shared_ptr <Node> nodeA;
		if (use_circle)
		{
			// Generate nodes in a circle...
			nodeA = std::make_shared<Node>(sf::Vector2f(cosf(i * degree) * radius + (screen_width / 2),
				sinf(i * degree) * radius + (screen_height / 2)));
		}
		else
		{
			// Generate nodes at random positions
			nodeA = std::make_shared<Node>(sf::Vector2f((rand() % (screen_width / 2)) + screen_width * 0.25,
				(rand() % (screen_height / 2)) + screen_height * 0.25));
		}

		// Add some nodes to the level
		auto display_object = std::dynamic_pointer_cast<Displayobject>(nodeA);
		mainLevel->display_objects.emplace_back(display_object);
	}

	// Setup Gene pool
	gene_pool = std::make_shared<Genepool>(mainLevel);

	// Get our first display path...(starting path to display)
	auto display_path = gene_pool->NextGeneration();

	// Add our display path...
	std::vector <std::shared_ptr<Connection>> connectors = display_path->get_connectors();
	mainLevel->display_objects.insert(mainLevel->display_objects.end(), connectors.begin(), connectors.end());

	// Setup font
	sf::Font font;
	if (!font.loadFromFile("arial.ttf"))
	{
		throw std::exception("Could not load font.");
	}

	// Setup future async
	std::future <std::shared_ptr<Path>> next_gen_operation;
	bool next_gen_set = true;

	// Setup upper text
	sf::Text upper_text;
	upper_text.setFont(font);
	upper_text.setString("Generation: 0");
	upper_text.setCharacterSize(48);
	upper_text.setFillColor(sf::Color::White);

	// Setup lower text
	sf::Text lower_text;
	lower_text.setFont(font);
	lower_text.setString("Node Count: " + std::to_string(node_count));
	lower_text.setCharacterSize(48);
	lower_text.setFillColor(sf::Color::White);
	lower_text.setPosition(0, screen_height * 0.9);

	// Track generations...
	int generation = 0;

	// Update our render while the window is open.
	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				window.close();
			}
		}

		window.clear();
		mainLevel->update();
		mainLevel->draw(window);
		window.draw(upper_text);
		window.draw(lower_text);

		// If we can setup for the next generation and the start timer is done
		if (next_gen_set == true && clock.getElapsedTime().asSeconds() > startup_wait_time_in_seconds)
		{
			next_gen_set = false;
			next_gen_operation = std::async([]
			{
				return gene_pool->NextGeneration();
			});
		}

		// If the operation is completed
		if (next_gen_operation._Is_ready())
		{
			// Increment generations
			generation++;
			upper_text.setString("Generation: " + std::to_string(generation) + " Best Path Length: " +
				std::to_string((int)display_path->get_length()));

			// Remove our display path...
			mainLevel->remove_all_connections(connectors);

			try
			{
				// Set display path
				auto value = next_gen_operation.get();
				display_path.swap(value);
			}
			catch (std::exception e)
			{
				std::cout << e.what() << std::endl;
			}

			// Set flag
			next_gen_set = true;

			// Add our next gen display path
			connectors = display_path->get_connectors();
			mainLevel->display_objects.insert(mainLevel->display_objects.end(), connectors.begin(), connectors.end());
		}

		window.display();
	}

	return 0;
}


