#include "Level.h"
#include <vector>
#include "Node.h"
#include <typeinfo>
#include <algorithm>
#include <gsl.h>
#include "Globals.h"

/// <summary>
/// Updates all the display objects in this level.
/// </summary>
void
Level::update()
{
	// Update all display objects
	for (int i = 0; i < display_objects.size(); i++)
	{
		gsl::at(display_objects, i)->update();
	}
}

/// <summary>
/// Draws all the display objects in this level.
/// </summary>
/// <param name="window">The window to render all display objects to.</param>
void
Level::draw(sf::RenderWindow& window)
{
	// Update all display objects
	for (int i = 0; i < display_objects.size(); i++)
	{
		gsl::at(display_objects, i)->draw(window);
	}
}

/// <summary>
/// Returns all Node type objects stored in
/// a vector of display objects this level holds.
/// </summary>
/// <returns>A vector of nodes.</returns>
std::vector <std::shared_ptr<Node>>
Level::get_all_nodes()
{
	std::vector <std::shared_ptr<Node>> nodes;
	nodes.reserve(VECTOR_RESERVATION);
	// Get all node type display objects and return them
	for (int i = 0; i < display_objects.size(); i++)
	{
		auto node = std::dynamic_pointer_cast<Node>(gsl::at(display_objects, i));
		// If it is type node...
		if (node)
		{
			nodes.emplace_back(node);
		}
	}

	return nodes;
}

/// <summary>
/// Returns all Connection type objects stored
/// in this level's display objects vector.
/// </summary>
/// <returns>A vector of Connection type objects</returns>
std::vector <std::shared_ptr<Connection>>
Level::get_all_connections()
{
	std::vector <std::shared_ptr<Connection>> connections;
	connections.reserve(VECTOR_RESERVATION);
	// Get all connection type display objects and return them
	for (int i = 0; i < display_objects.size(); i++)
	{
		auto connection = std::dynamic_pointer_cast<Connection>(gsl::at(display_objects, i));
		if (connection)
		{
			connections.emplace_back(connection);
		}
	}

	return connections;
}

/// <summary>
/// Given a list of connection object pointers this erases them from
/// its display objects list effectively removing them from the level.
/// </summary>
/// <param name="connections">A vector of connection pointers to remove from this level.</param>
void
Level::remove_all_connections(std::vector <std::shared_ptr<Connection>> connections)
{
	for (int i = display_objects.size() - 1; i >= 0; i--)
	{
		auto connection = std::dynamic_pointer_cast<Connection>(gsl::at(display_objects, i));
		// If our vector of items to remove contains the connection found in display objects
		if (std::find(connections.begin(), connections.end(), connection) != connections.end())
		{
			// Erase this item
			display_objects.erase(display_objects.begin() + i);
		}
	}
}
