#pragma once

#include "DisplayObject.h"
#include <SFML\Graphics\CircleShape.hpp>
#include "Utility"
#include <gsl.h>

/// <summary>
/// The Node class is for displaying nodes of a graph
/// or map on screen. Each node holds a shape (default circle)
/// which it will draw on screen when its draw() method is called.
/// </summary>
class Node : public Displayobject
{
public:
	// Functions
	Node(sf::Vector2f position);

	void
	update() override;

	void
	draw(sf::RenderWindow& window) override;

	sf::Vector2f
	get_center() override;

	void
	set_position(sf::Vector2f position) override;

	sf::Color
	get_color();

private:

	// Variables 
	sf::CircleShape shape;
};

