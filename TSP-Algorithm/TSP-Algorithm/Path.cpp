#include "Path.h"
#include "Utility.h"
#include <algorithm>
#include <gsl.h>
#include "Globals.h"
#include <limits>

/// <summary>
/// Constructs a new path of nodes.
/// </summary>
/// <param name="nodes">A vector of nodes.</param>
/// <param name="nearest_neighbor">Determines if the new path should use a form of nearest neighbor to help generate a better path.</param>
Path::Path(std::vector <std::shared_ptr<Node>> nodes, bool nearest_neighbor)
{
	if (nodes.size() == 0)
	{
		throw std::exception("Not enough nodes to generate a path.");
	}

	// Randomise our node path
	std::random_shuffle(nodes.begin(), nodes.end());

	// Set our nodes path.
	this->path_nodes.clear();
	this->path_nodes = nodes;

	if (nearest_neighbor == true)
	{
		nearest_neighbor_pass(nodes.size());
	}

	// Snake our path together (like a snake eating its tail)
	this->path_nodes.emplace_back(gsl::at(nodes, 0));

}

/// <summary>
/// Constructs a path of nodes by combining two already-existing paths.
/// These parents are combined to create a child path.
/// </summary>
/// <param name="nodes">Nodes that our path can be made of (all nodes in a level).</param>
/// <param name="pathA">The first parent path.</param>
/// <param name="pathB">The second parent path.</param>
Path::Path(std::vector <std::shared_ptr<Node>> nodes, std::shared_ptr <Path> pathA, std::shared_ptr <Path> pathB)
{
	if (pathA->path_nodes.size() != pathB->path_nodes.size())
	{
		throw std::exception("Path lengths do not match!");
	}

	// Mix the paths given to us
	std::vector <std::shared_ptr<Node>> mixedPath;
	std::vector <std::shared_ptr<Node>> path_a;
	std::vector <std::shared_ptr<Node>> path_b;

	// Reserve space
	mixedPath.reserve(VECTOR_RESERVATION);
	path_a.reserve(VECTOR_RESERVATION);
	path_b.reserve(VECTOR_RESERVATION);

	// Copy pathA and pathB so we can modify them without consequences
	for (int i = 0; i < pathA->path_nodes.size(); i++)
	{
		path_a.emplace_back(gsl::at(pathA->path_nodes, i));
		path_b.emplace_back(gsl::at(pathB->path_nodes, i));
	}

	// Remove the trailing node that wraps the path back together
	path_a.pop_back();
	path_b.pop_back();
	std::vector <std::shared_ptr<Node>> unused_nodes;
	unused_nodes.reserve(VECTOR_RESERVATION);
	// Add pairs that are in both paths
	for (int i = 1; i < path_a.size(); i++)
	{
		if (contains_pair(path_a, gsl::at(path_a, i - 1), gsl::at(path_a, i))
			&& contains_pair(path_b, gsl::at(path_a, i - 1), gsl::at(path_a, i)))
		{
			mixedPath.emplace_back(gsl::at(path_a, i - 1));
			mixedPath.emplace_back(gsl::at(path_a, i));
			path_a.erase(path_a.begin() + i - 1);
			path_a.erase(path_a.begin() + i - 1);
			i--;
		}
	}

	// Shuffle unused nodes...
	std::random_shuffle(path_a.begin(), path_a.end());
	std::random_shuffle(path_b.begin(), path_b.end());

	// Append path b to path a vector
	path_a.insert(path_a.end(), path_b.begin(), path_b.end());

	// Add remaining nodes...
	for (int i = 0; i < path_a.size(); i++)
	{
		// Insert nodes in-between pairs...
		int insert_position = 0;

		if (mixedPath.size() != 0)
		{
			insert_position = rand() % mixedPath.size();
		}

		// If mixed path does not contain this node yet...insert at random position
		if (std::count(mixedPath.begin(), mixedPath.end(), gsl::at(path_a, i)) == 0)
		{
			// Insert into mixed path...
			mixedPath.insert(mixedPath.begin() + insert_position, gsl::at(path_a, i));
		}
	}

	// Add the trailing node that wraps the path back together
	mixedPath.emplace_back(gsl::at(mixedPath, 0));

	// Set our nodes path.
	this->path_nodes = mixedPath;

}

/// <summary>
/// Gets the length of a path.
/// </summary>
/// <returns>Returns the length of this path.</returns>
float
Path::get_length()
{
	float length = 0.0f;
	for (int i = 1; i < path_nodes.size(); i++)
	{
		length += get_distance(gsl::at(path_nodes, i - 1)->get_center(), gsl::at(path_nodes, i)->get_center());
	}
	return length;
}

/// <summary>
/// Gets a vector of connections.
/// </summary>
/// <returns>Gets a vector of connections for display.</returns>
std::vector <std::shared_ptr<Connection>>
Path::get_connectors()
{
	connectors.clear();

	for (int i = 1; i < path_nodes.size(); i++)
	{
		connectors.emplace_back(std::make_shared<Connection>(gsl::at(path_nodes, i - 1), gsl::at(path_nodes, i)));
	}

	return connectors;
}

/// <summary>
/// Gets a copy of this path.
/// </summary>
/// <returns>Returns a copy of this path.</returns>
std::shared_ptr <Path>
Path::get_copy()
{
	auto path_copy = std::make_shared<Path>(this->path_nodes, false);

	path_copy->path_nodes.clear();

	for (int i = 0; i < this->path_nodes.size(); i++)
	{
		path_copy->path_nodes.emplace_back(gsl::at(this->path_nodes, i));
	}

	path_copy->connectors.clear();

	return path_copy;
}

/// <summary>
/// Gets the starting node of the longest node pair.
/// e.g. The longest distance between two nodes.
/// </summary>
/// <returns>Returns the starting node of the longest node pair.</returns>
std::shared_ptr <Node>
Path::longest_node()
{
	float longest_distance = -1.0f;
	std::shared_ptr <Node> longest_start_node;
	for (int i = 1; i < path_nodes.size(); i++)
	{
		auto start = gsl::at(path_nodes, i - 1);
		auto end = gsl::at(path_nodes, i);
		const float distance = get_distance(start->get_center(), end->get_center());
		if (distance > longest_distance)
		{
			longest_distance = distance;
			longest_start_node = gsl::at(path_nodes, i - 1);
		}
	}

	return longest_start_node;
}

/// <summary>
/// Removes a node from this path.
/// </summary>
/// <param name="node">The node to remove from this path.</param>
void
Path::remove_node(std::shared_ptr <Node> node)
{
	const int position = get_position(node);
	path_nodes.erase(path_nodes.begin() + position);
}

/// <summary>
/// Gets the longest distance
/// </summary>
/// <returns>The longest distance found between two nodes.</returns>
float
Path::longest_distance()
{
	float longest_distance = -1.0f;
	auto longest_start_node = gsl::at(path_nodes, 0);
	for (int i = 1; i < path_nodes.size(); i++)
	{
		const float distance = get_distance(gsl::at(path_nodes, i - 1)->get_center(),
			gsl::at(path_nodes, i)->get_center());
		if (distance > longest_distance)
		{
			longest_distance = distance;
			longest_start_node = gsl::at(path_nodes, i - 1);
		}
	}

	return longest_distance;
}

/// <summary>
/// Performs a single pass trying to swap out nodes so that
/// the longest path is replaced with a shorter one. When done
/// many multiple times this will result in a greedy path.
/// This is used sparingly in our Genetic Algorithm to help
/// eliminate stupid routes.
/// </summary>
/// <param name="amount">The amount of passes to perform.</param>
void
Path::nearest_neighbor_pass(int amount)
{
	for (int i = 0; i < amount; i++)
	{
		auto mismatch = get_greatest_mismatch();

		if (mismatch == nullptr)
		{ return; } // Abort

		auto nearest_neighbor = get_closest_node(mismatch);

		if (nearest_neighbor == nullptr) return; // Abort

		const int position = get_position(mismatch) + 1;

		if (position < path_nodes.size() - 1)
		{
			swap(position, get_position(nearest_neighbor));
		}
	}
}

/// <summary>
/// Returns the position of a node inside this path's list of nodes.
/// </summary>
/// <param name="node">The index a node is located at.</param>
/// <returns></returns>
int
Path::get_position(std::shared_ptr <Node> node)
{
	auto iterator = std::find(path_nodes.begin(), path_nodes.end(), node);
	return std::distance(path_nodes.begin(), iterator);
}

/// <summary>
/// Gets the closest node to a given node.
/// </summary>
/// <param name="node">The node to search with.</param>
/// <returns>The closest node to the one provided.</returns>
std::shared_ptr <Node>
Path::get_closest_node(std::shared_ptr <Node> node)
{
	float min_distance = std::numeric_limits<float>::max();
	std::shared_ptr <Node> closest_node = nullptr;
	for (int i = 0; i < path_nodes.size(); i++)
	{
		if (gsl::at(path_nodes, i) != node)
		{
			const float distance = get_distance(node->get_center(), gsl::at(path_nodes, i)->get_center());

			if (distance < min_distance)
			{
				min_distance = distance;
				closest_node = gsl::at(path_nodes, i);
			}
		}
	}

	return closest_node;
}

/// <summary>
/// Finds the node that has the greatest lost potential,
/// when comparing its closest neighbor to its actual attatched node.
/// </summary>
/// <returns>The node with the most lost potential.</returns>
std::shared_ptr <Node>
Path::get_greatest_mismatch()
{
	float lost_potential = 0;

	std::shared_ptr <Node> mismatch = nullptr;
	for (int i = 1; i < this->path_nodes.size(); i++)
	{
		const std::shared_ptr <Node> node = gsl::at(this->path_nodes, i - 1);
		const std::shared_ptr <Node> closest_node = get_closest_node(node);
		const std::shared_ptr <Node> connected_node = gsl::at(this->path_nodes, i);

		if (&closest_node != &connected_node)
		{
			const float to_closest_node = get_distance(node->get_center(), closest_node->get_center());
			const float to_connected_node = get_distance(node->get_center(), connected_node->get_center());
			const float difference = to_connected_node - to_closest_node;

			if (difference > lost_potential)
			{
				lost_potential = difference;
				mismatch = node;
			}
		}
	}

	return mismatch;
}

/// <summary>
/// Swaps two nodes in this path.
/// </summary>
/// <param name="a">The index of the first node.</param>
/// <param name="b">The index of the second node.</param>
void
Path::swap(int a, int b)
{
	// Swap two random elements in the path
	std::iter_swap(this->path_nodes.begin() + a, this->path_nodes.begin() + b);
}

/// <summary>
/// Checks if a path contains a pair of nodes (in order).
/// </summary>
/// <param name="path">The path to check.</param>
/// <param name="a">The first node in the pair.</param>
/// <param name="b">The second node in the pair.</param>
/// <returns>True if the path contains the nodes given in order (a then b).</returns>
bool
Path::contains_pair(std::vector <std::shared_ptr<Node>> path, std::shared_ptr <Node> a, std::shared_ptr <Node> b)
{
	for (int i = 1; i < path.size(); i++)
	{
		auto first = gsl::at(path, i - 1);
		auto second = gsl::at(path, i);

		if (first == a && second == b)
		{
			return true;
		}
	}

	return false;
}
