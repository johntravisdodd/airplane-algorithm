#include "GenePool.h"
#include "Path.h"
#include "Connection.h"
#include "Level.h"
#include <algorithm>
#include <gsl.h>
#include "Globals.h"

Genepool::Genepool(std::shared_ptr <Level> mainLevel)
{
	main_level = mainLevel;

	// Generate initial random paths and add them to the gene pool...
	for (int i = 0; i < initial_generation_size; i++)
	{
		gene_pool.emplace_back(std::make_shared<Path>(main_level->get_all_nodes(), true));
	}

	// Sort the gene pool...
	sort_gene_pool();

	// Pick the top x amount
	std::vector <std::shared_ptr<Path>> best_genes;
	best_genes.reserve(VECTOR_RESERVATION);
	for (int i = 0; i < max_gene_pool_size; i++)
	{
		best_genes.emplace_back(gsl::at(gene_pool, i));
	}

	gene_pool = best_genes;

}

std::shared_ptr <Path>
Genepool::NextGeneration()
{
	// Add salt to the gene pool (random genes)
	for (int i = 0; i < fresh_genes_amount; i++)
	{
		auto path = std::make_shared<Path>(main_level->get_all_nodes(), false);
		this->gene_pool.emplace_back(path);
	}

	// Copy the top path and shuffle it...
	for (int i = 0; i < shuffle_amount; i++)
	{
		auto top_performer = gsl::at(this->gene_pool, 0)->get_copy();

		top_performer->path_nodes.pop_back();
		std::vector <std::shared_ptr<Node>> long_nodes;
		long_nodes.reserve(VECTOR_RESERVATION);
		// Swap long nodes...
		auto long_node = top_performer->longest_node();

		// 1 in 12 we get random node
		if (rand() % 12 == 0)
		{
			long_node = gsl::at(top_performer->path_nodes, rand() % top_performer->path_nodes.size());
		}

		long_nodes.emplace_back(long_node);
		const int position = top_performer->get_position(long_node);
		top_performer->swap(position, rand() % top_performer->path_nodes.size());

		top_performer->path_nodes.emplace_back(gsl::at(top_performer->path_nodes, 0));

		this->gene_pool.emplace_back(top_performer);

	}

	// Breed top performers with random selection from pool
	const int pool_size = this->gene_pool.size();
	for (int i = 0; i < pool_size; i++)
	{
		auto mistress = gsl::at(this->gene_pool, rand() % breed_core)->get_copy();
		auto scoundral = gsl::at(this->gene_pool, i)->get_copy();

		auto child = std::make_shared<Path>(main_level->get_all_nodes(), scoundral, mistress);

		if (rand() % 3 == 0)
		{
			child->nearest_neighbor_pass(rand() % 12);
		}

		this->gene_pool.emplace_back(child);
	}

	// Nearest neighbor optimization for top performer
	std::shared_ptr <Path> top = gsl::at(this->gene_pool, 0)->get_copy();
	top->nearest_neighbor_pass(rand() % 3 + 1);
	this->gene_pool.emplace_back(top);

	// Sort our gene pool
	sort_gene_pool();

	// Limit our gene pool size
	this->gene_pool = std::vector < std::shared_ptr <
		Path >> (this->gene_pool.begin(), this->gene_pool.begin() + max_gene_pool_size);

	// Return best path...
	return gsl::at(gene_pool, 0);
}

void
Genepool::sort_gene_pool()
{
	std::sort(gene_pool.begin(), gene_pool.end(), [](std::shared_ptr <Path> a, std::shared_ptr <Path> b)
	{
		return a->get_length() < b->get_length();
	});
}

