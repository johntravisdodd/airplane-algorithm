#include "Node.h"
#include "Utility.h"
#include <gsl.h>

/// <summary>
/// Creates a new node at the specified coordinates.
/// </summary>
/// <param name="position">The position where the node will be located.</param>
Node::Node(sf::Vector2f position)
{
	shape = sf::CircleShape(5.0f);
	shape.setFillColor(get_random_color());
	shape.setPosition(position);
}

/// <summary>
/// Nodes do currently do anything during update();
/// </summary>
void
Node::update()
{
	// GNDN
}

/// <summary>
/// Draws the node on screen.
/// </summary>
/// <param name="window">The render window to draw this node to.</param>
void
Node::draw(sf::RenderWindow& window)
{
	// Draw our shape on screen
	window.draw(shape);
}

/// <summary>
/// Returns the center of the node.
/// </summary>
/// <returns>The center of this node's shape.</returns>
sf::Vector2f
Node::get_center()
{
	// Add half of radius to get actual center (position is in upper left)
	const float x = shape.getPosition().x + (shape.getRadius());
	const float y = shape.getPosition().y + (shape.getRadius());
	sf::Vector2f center = sf::Vector2f(x, y);
	return center;
}

/// <summary>
/// Sets the position of this node.
/// </summary>
/// <param name="position">The position to move the node to.</param>
void
Node::set_position(sf::Vector2f position)
{
	shape.setPosition(position);
}

/// <summary>
/// Returns the color of this node's shape.
/// </summary>
/// <returns>A sf::Color type.</returns>
sf::Color
Node::get_color()
{
	return shape.getFillColor();
}
