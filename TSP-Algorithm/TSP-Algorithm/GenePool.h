#pragma once

#include "Path.h"
#include <vector>
#include <gsl.h>

/// <summary>
/// Handles genetic algorithm portion of the program.
/// This returns the best path of each generation and
/// holds default settings related to solving the TSP problem.
/// </summary>
class Genepool
{
public:
	Genepool(std::shared_ptr <Level> mainLevel);

	std::shared_ptr <Path>
	NextGeneration();

private:

	void
	sort_gene_pool();

	// Variables
	std::vector <std::shared_ptr<Path>> gene_pool;
	std::shared_ptr <Level> main_level = nullptr;
	int initial_generation_size = 250;
	int max_gene_pool_size = 50;
	int fresh_genes_amount = 5;
	int breed_core = 50;
	int shuffle_amount = 650;

};

