#pragma once

#include "DisplayObject.h"
#include <vector>
#include "Connection.h"
#include <gsl.h>

/// <summary>
/// Holds a list of display_object pointers and calls their
/// update and draw methods. Designed to hold any abstraction
/// of Displayobject so it is class agnostic in that sense.
/// However this also holds  methods for retrieving specific
/// class types from its list of Displayobjects.
/// </summary>
class Level
{
public:

	// Functions
	void
	update();

	void
	draw(sf::RenderWindow& window);

	std::vector <std::shared_ptr<Node>>
	get_all_nodes();

	std::vector <std::shared_ptr<Connection>>
	get_all_connections();

	void
	remove_all_connections(std::vector <std::shared_ptr<Connection>> connections);

	// Variables
	std::vector <std::shared_ptr<Displayobject>> display_objects = std::vector < std::shared_ptr < Displayobject >> ();
};

