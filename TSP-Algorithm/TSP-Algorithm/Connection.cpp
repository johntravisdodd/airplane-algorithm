#include "Connection.h"
#include "Utility.h"
#include <gsl.h>

/// <summary>
/// Constructs a new connection between node a and node b.
/// Draws a line between these two when called in a draw loop.
/// </summary>
/// <param name="a">The starting node to draw from.</param>
/// <param name="b">The ending node to draw to.</param>
Connection::Connection(std::shared_ptr <Node> a, std::shared_ptr <Node> b)
{
	A = a;
	B = b;
	Color_a = a->get_color();
	Color_b = b->get_color();
}

/// <summary>
/// Updates the connection so that it draws a line
/// to the current position of its nodes.
/// </summary>
void
Connection::update()
{
	// Create a line from point A to point B
	const sf::Vector2f positionA = A->get_center();
	const sf::Vector2f positionB = B->get_center();
	const sf::Vertex vertexA = sf::Vertex(positionA, Color_a);
	const sf::Vertex vertexB = sf::Vertex(positionB, Color_b);

	// Convert vertext points to array
	sf::VertexArray vertexPoints(sf::Lines, 2);
	vertexPoints.append(vertexA);
	vertexPoints.append(vertexB);

	// Set the array which will be drawn
	Vertex_Array = vertexPoints;

}

/// <summary>
/// Draws the connection line between
/// two nodes on-screen.
/// </summary>
/// <param name="window">The render window to draw to.</param>
void
Connection::draw(sf::RenderWindow& window)
{
	window.draw(Vertex_Array);
}

/// <summary>
/// Returns the first node's position.
/// </summary>
/// <returns>The position of the first node.</returns>
sf::Vector2f
Connection::get_center()
{
	return this->A->get_center();
}

/// <summary>
/// Will throw an exception. A Connection's position
/// is defined purely in terms of its nodes. It does
/// not control positioning in any way shape or form.
/// </summary>
/// <param name="position">The position you wish to set.</param>
void
Connection::set_position(sf::Vector2f position)
{
	throw std::exception("Function not implemented.");
}
